import { NgModule } from "@angular/core";
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';

@NgModule({
  imports: [
    NzImageModule,
    NzCarouselModule

  ],
  declarations: [
  ],
  exports: [
    NzImageModule,
    NzCarouselModule
  ]
})
export class SharedModule { }