import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpHeaders,
  HttpResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  constructor(private http: HttpClient) { }

  private _loading = false;

  /** 是否正在加载中 */
  get loading(): boolean {
    return this._loading;
  }

  parseParams(params: {}): HttpParams {
    const newParams = {};
    Object.keys(params).forEach(key => {
      let _data = params[key];
      // 忽略空值
      if (_data == null) {
        return;
      }
      // 将时间转化为：时间戳 (秒)
      if (_data instanceof Date) {
        _data = _data.valueOf();
      }
      newParams[key] = _data;
    });
    return new HttpParams({ fromObject: newParams });
  }

  begin() {
    // console.time('http');
    setTimeout(() => (this._loading = true));
  }

  end() {
    // console.timeEnd('http');
    setTimeout(() => (this._loading = false));
  }

  post(
    url: string,
    body: any,
    params: any,
    options: {
      headers?: HttpHeaders | { [header: string]: string | string[] };
      observe?: 'body' | 'events' | 'response';
      reportProgress?: boolean;
      responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
      withCredentials?: boolean;
    }
  ): Observable<any> {
    return this.request('POST', url, {
      body,
      params,
      ...options
    });
  }

  /**
   * `request` 请求
   *
   * @param method 请求方法类型
   * @param url URL地址
   * @param options 参数
   */
  request(
    method: string,
    url: string,
    options?: {
      body?: any;
      headers?:
      | HttpHeaders
      | {
        [header: string]: string | string[];
      };
      observe?: 'body' | 'events' | 'response';
      params?:
      | HttpParams
      | {
        [param: string]: string | string[];
      };
      responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
      reportProgress?: boolean;
      withCredentials?: boolean;
    }
  ): Observable<any> {
    this.begin();
    if (options) {
      if (options.params) options.params = this.parseParams(options.params);
    }
    return this.http.request(method, url, options).pipe(
      tap(() => {
        this.end();
      }),
      catchError(res => {
        this.end();
        return throwError(res);
      })
    );
  }
}
