import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { WechatAuthService } from './wechat-auth.service';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class StartupService {
  private http: HttpClient;
  private pathArray: any;
  constructor(
    private wechatAuth: WechatAuthService,
    private router: Router,
    handler: HttpBackend,
  ) {
    this.http = new HttpClient(handler);
  }

  load(): Promise<any> {
    return new Promise((resolve, reject) => {
      const reg = /\?code=([\w]+)/;
      if (reg.test(location.search)) {
        const code = reg.exec(location.search)[0];
        console.log(code);
        resolve(null);
      }
      else {
        this.router.navigateByUrl('/index');
        resolve(null);
      }
    });
  }

  private redirectToWechatAuth() {
    debugger;
    const wechatAuth = this.wechatAuth.getWechatAuth();
    location.href = wechatAuth;
  }
}
