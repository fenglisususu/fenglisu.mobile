import {
  HttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HttpResponseBase,
  HttpResponse,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { mergeMap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClientService } from './http-client.service';
@Injectable()
export class DefaultInterceptor implements HttpInterceptor {
  constructor(
    private injector: Injector,
  ) { }

  private handleData(event: HttpResponseBase): Observable<any> {
    // 可能会因为 `throw` 导出无法执行 `_HttpClient` 的 `end()` 操作
    if (event.status > 0) {
      this.injector.get(HttpClientService).end();
    }
    // this.checkStatus(event);
    // 业务处理：一些通用操作
    switch (event.status) {
      case 200:
        // 业务层级错误处理，以下是假定restful有一套统一输出格式（指不管成功与否都有相应的数据格式）情况下进行处理
        // 例如响应内容：
        //  错误内容：{ status: 1, msg: '非法参数' }
        //  正确内容：{ status: 0, response: {  } }
        // 则以下代码片断可直接适用
        if (event instanceof HttpResponse) {
          const body: any = event.body;
          if (body && !body.success) {
            if (body.errCode !== '403') {
              // if (body.msg) this.toast.fail(body.msg);
              // 继续抛出错误中断后续所有 Pipe、subscribe 操作，因此：
              // this.http.get('/').subscribe() 并不会触发
            } else {
              // 未登录直接跳转
              // const wechatAuth = this.wechatAuth.getWechatAuth();
              // location.href = wechatAuth;
            }

            return throwError({});
          } else {
            // 重新修改 `body` 内容为 `response` 内容，对于绝大多数场景已经无须再关心业务状态码
            return of(new HttpResponse(Object.assign(event, { body: body.model })));
            // 或者依然保持完整的格式
            return of(event);
          }
        }
        break;
      default:
        if (event instanceof HttpErrorResponse) {
          console.warn('未可知错误，大部分是由于后端不支持CORS或无效配置引起', event);
          return throwError(event);
        }
        break;
    }
    return of(event);
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // 统一加上服务端前缀
    let url = req.url;
    if (!url.startsWith('https://') && !url.startsWith('http://')) {
      url = environment.SERVER_URL + url;
    }

    const headers = new HttpHeaders({
      // token: this.tokenService.get()
    });

    const newReq = req.clone({ url, headers });
    return next.handle(newReq).pipe(
      mergeMap((event: any) => {
        // 允许统一对请求错误处理
        if (event instanceof HttpResponseBase) return this.handleData(event);
        // 若一切都正常，则后续操作
        return of(event);
      }),
      catchError((err: HttpErrorResponse) => this.handleData(err))
    );
  }
}
