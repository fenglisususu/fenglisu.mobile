import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class WechatAuthService {
  constructor(private location: Location) { }

  public getWechatAuth() {
    const redirectUri = `${location.origin}${location.pathname}`;
    const wechatAuth = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx0e3cae754f37fecb&redirect_uri=${encodeURIComponent(
      redirectUri
    )}&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect`;
    return wechatAuth;
  }
}