import { Component, OnInit } from '@angular/core';
import { BannerComponent } from './banner/banner.component';
import { Widget } from './widget';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.less']
})
export class IndexComponent implements OnInit {
  data = { widgets: new Array<Widget>() };
  widgets = {
    banner: BannerComponent,
  };
  inputs = {
    hello: 'world',
    something: () => 'can be really complex',
  };
  constructor() {
    this.data.widgets.push({ typeCode: 'banner' });
  }

  ngOnInit(): void {
  }

  getComponent(widget: Widget) {
    return this.widgets[widget.typeCode];
  }
}
