import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatComponent } from './cat/cat.component';
import { IndexComponent } from './index/index.component';
import { environment } from 'src/environments/environment';

const routes: Routes = [
  { path: 'cat', component: CatComponent },
  { path: 'index', component: IndexComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: environment.useHash, })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
